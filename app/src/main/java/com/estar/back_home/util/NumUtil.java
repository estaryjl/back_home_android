package com.estar.back_home.util;

public class NumUtil {
    public static String GetCH(int num){
        String CHNum = "";
        switch (num){
            case 1:
                CHNum = "一";
                break;
            case 2:
                CHNum = "二";
                break;
            case 3:
                CHNum = "三";
                break;
            case 4:
                CHNum = "四";
                break;
            case 5:
                CHNum = "五";
                break;
            case 6:
                CHNum = "六";
                break;
            case 7:
                CHNum = "七";
                break;
            case 8:
                CHNum = "八";
                break;
            case 9:
                CHNum = "九";
                break;
            case 10:
                CHNum = "十";
                break;
        }

        return CHNum;
    }
}

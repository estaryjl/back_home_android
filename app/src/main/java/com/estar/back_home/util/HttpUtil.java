package com.estar.back_home.util;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class HttpUtil {

    public static String token = "";

    public static void sendOkHttpPostRequest(String address, RequestBody requestBody, okhttp3.Callback callback){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(address)
                .post(requestBody)
                .header("Token", token)
                .build();
        client.newCall(request).enqueue(callback);
    }

    public static void sendOkHttpGetRequest(String address, okhttp3.Callback callback){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(address)
                .get()
                .header("Token", token)
                .build();
        client.newCall(request).enqueue(callback);
    }
    public static void sendOkHttpPutRequest(String address, RequestBody requestBody, okhttp3.Callback callback){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(address)
                .put(requestBody)
                .header("Token", token)
                .build();
        client.newCall(request).enqueue(callback);
    }
}

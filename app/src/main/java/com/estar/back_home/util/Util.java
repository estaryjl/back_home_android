package com.estar.back_home.util;

import android.content.Context;
import android.content.SharedPreferences;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Util {
    /**
     * 分割list
     * @param list
     * @return
     */
    public static List<List<String>> groupList(List<String> list){
        List<List<String>> listGroup = new ArrayList<List<String>>();
        int listSize = list.size();
        int toIndex = 4;

        for( int i = 0; i <list.size(); i +=4){
            if( i + 4 > listSize){//

                if( listSize - i == 3){
                    toIndex = 3;
                }
                if( listSize - i == 2){
                    toIndex = 2;
                }
                else if(listSize - i ==1){
                    toIndex = 1;
                }

            }
            List<String> newList = list.subList(i,i+ toIndex);
            listGroup.add(newList);
        }
        return listGroup;
    }
    /**
     * 从value拿key
     */
    public static String getKey(HashMap<String, String> map, String value){
        String key = null;
        //Map,HashMap并没有实现Iteratable接口.不能用于增强for循环.
        for(String getKey: map.keySet()){
            if(map.get(getKey).equals(value)){
                key = getKey;
            }
        }
        return key;
        //这个key肯定是最后一个满足该条件的key.
    }

    /**
     * 把字符串存到本地 SharedPrefrences
     */
    public static void saveSettingNote(Context context, String filename , Map<String, String> map){
        SharedPreferences.Editor note = context.getSharedPreferences(filename, Context.MODE_PRIVATE).edit();
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry<String, String> entry = (Map.Entry<String, String>)it.next();
            note.putString(entry.getKey(),entry.getValue());
        }
        note.commit();
    }
    public static String getSettingNote(Context context, String filename, String datename){
        SharedPreferences read = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
        return read.getString(datename,null);
    }








}

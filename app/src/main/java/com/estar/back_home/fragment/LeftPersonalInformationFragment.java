package com.estar.back_home.fragment;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.estar.back_home.R;
import com.suke.widget.SwitchButton;
import com.xuexiang.xui.widget.imageview.RadiusImageView;



public class LeftPersonalInformationFragment extends Fragment {
    private RadiusImageView headImageV;
    private TextView nickNameEdx;
    private TextView statusEdx,nameEdx,sexEdx,areaEdx,timeLengthEdx;

    private SwitchButton changeStatusSwitchBtn;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_left_personalinformation,container,false);
        init(view);

        changeStatusSwitchBtn.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(isChecked == true){
                    getToast(true);
                    statusEdx.setText("可出任务");
                }else {
                    getToast(false);
                    statusEdx.setText("停出任务");
                }
            }
        });

        return view;


    }

    private void getToast(Boolean isOk){
        if(isOk){
            Toast toast = Toast.makeText(getActivity(),"状态已被修改为\"可出任务\"",Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP,0,0);
            toast.show();
        }else {
            Toast toast = Toast.makeText(getActivity(),"状态已被修改为\"停出任务\"",Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP,0,0);
            toast.show();
        }
    }

    private void init(View view){
        headImageV = view.findViewById(R.id.head_image_imageview);
        nickNameEdx = view.findViewById(R.id.nickname_textview);
        statusEdx = view.findViewById(R.id.personal_status_tv);
        nameEdx = view.findViewById(R.id.name_edx);
        sexEdx = view.findViewById(R.id.sex_edx);
        areaEdx = view.findViewById(R.id.area_edx);
        timeLengthEdx = view.findViewById(R.id.time_edx);
        changeStatusSwitchBtn = view.findViewById(R.id.status_change_switch);


    }
}

package com.estar.back_home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.estar.back_home.applications.TrackApplication;
import com.estar.back_home.util.BitmapUtil;
import com.estar.back_home.util.Constants;
import com.estar.back_home.util.SharedPreferenceUtil;
import com.xuexiang.xui.XUI;
import com.xuexiang.xui.widget.button.ButtonView;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;


public class LoginActivity extends AppCompatActivity {

    EditText accountEdx,passwordEdx;
    ButtonView loginBtn;
    private String permissionInfo;

    //做一些必要的初始化
    private TrackApplication trackApp;

    /**权限**/
    String[] permissions = {Manifest.permission.WRITE_SETTINGS};

    private final int SDK_PERMISSION_REQUEST = 127;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        BitmapUtil.init();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,ActingActivity.class);
                startActivity(intent);
            }
        });
        getPersimmions();
    }

    /**初始化组件**/
    private void init(){
        accountEdx = findViewById(R.id.account_edx_main);
        passwordEdx = findViewById(R.id.pwd_edx_main);
        loginBtn = findViewById(R.id.login_btn_main);

        //一些必要的初始化
        trackApp = (TrackApplication) getApplicationContext();
        //获取权限
        boolean isContainsPermissionKey = SharedPreferenceUtil.contains(this, Constants.PERMISSIONS_DESC_KEY);
        if (!isContainsPermissionKey) {
            showDialog();
        } else {
            boolean isAccessPermission = SharedPreferenceUtil
                    .getBoolean(this, Constants.PERMISSIONS_DESC_KEY, false);
            if (!isAccessPermission) {
                showDialog();
            }
        }
    }


    /**
     * 显示提示信息
     */
    private void showDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示: ");
        TextView textView = new TextView(this);
        textView.setText(R.string.privacy_permission_desc);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setPadding( 50 , 30 , 50 , 10 );
        builder.setView(textView);

        builder.setPositiveButton("同意", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferenceUtil
                        .putBoolean(LoginActivity.this, Constants.PERMISSIONS_DESC_KEY, true);
                dialog.cancel();
            }
        });

        builder.setNegativeButton("不同意", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferenceUtil
                        .putBoolean(LoginActivity.this, Constants.PERMISSIONS_DESC_KEY, false);
                dialog.cancel();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    @TargetApi(23)
    private void getPersimmions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> permissions = new ArrayList<String>();
            /***
             * 定位权限为必须权限，用户如果禁止，则每次进入都会申请
             */
            // 定位精确位置
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            /*
             * 读写权限和电话状态权限非必要权限(建议授予)只会申请一次，用户同意或者禁止，只会弹一次
             */
            // 读写权限
            if (addPermission(permissions, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissionInfo += "Manifest.permission.WRITE_EXTERNAL_STORAGE Deny \n";
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this,permissionInfo,Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if (permissions.size() > 0) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), SDK_PERMISSION_REQUEST);
            }
        }
    }

    @TargetApi(23)
    private boolean addPermission(ArrayList<String> permissionsList, String permission) {
        // 如果应用没有获得对应权限,则添加到列表中,准备批量申请
        /**shouldShowRequestPermissionRationale
         * 1，在允许询问时返回true ； 2，在权限通过 或者权限被拒绝并且禁止询问时返回false
         * 但是有一个例外，就是重来没有询问过的时候，也是返回的false 所以单纯的使用shouldShowRequestPermissionRationale去做什么判断，是没用的，只能在请求权限回调后再使用。
         * Google的原意是：
         * 1，没有申请过权限，申请就是了，所以返回false； 2，申请了用户拒绝了，那你就要提示用户了，所以返回true；
         * 3，用户选择了拒绝并且不再提示，那你也不要申请了，也不要提示用户了，所以返回false； 4，已经允许了，不需要申请也不需要提示，所以返回false；
        **/
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(permission)) {//允许询问 返回true
                return true;
            } else {
                permissionsList.add(permission);
                return false;
            }
        } else {
            return false;//表明已经拿到权限了 不需要添加了
        }
    }
    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // TODO Auto-generated method stub
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }




}
